package co.za.amahle.studentregisterlib;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.google.firebase.FirebaseApp;

/**
 * Created by sifiso.mtshweni on 2018/06/07.
 */
public class StudentRegisterApp extends Application {
    public static final String TAG = StudentRegisterApp.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();
        StringBuilder sb = new StringBuilder();
        sb.append("#############################################\n");
        sb.append("######## SUBSCRIBER APP started");
        sb.append("#############################################\n");
        Log.d(TAG, sb.toString());
        FirebaseApp.initializeApp(this);
        Log.w(TAG, "onCreate: StudentRegisterApp initializeApp complete");
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

}
