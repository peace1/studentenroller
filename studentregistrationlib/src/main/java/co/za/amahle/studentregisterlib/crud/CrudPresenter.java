package co.za.amahle.studentregisterlib.crud;


import co.za.amahle.studentregisterlib.api.DataAPI;
import co.za.amahle.studentregisterlib.api.ListAPI;
import co.za.amahle.studentregisterlib.model.Response;
import co.za.amahle.studentregisterlib.model.Student;

/**
 * Created by sifiso.mtshweni on 2018/06/07.
 */

public class CrudPresenter implements CrudContract.Presenter {
    private CrudContract.View view;
    private DataAPI dataAPI;
    private ListAPI listAPI;

    public CrudPresenter(CrudContract.View view) {
        this.view = view;
        dataAPI = new DataAPI();
        listAPI = new ListAPI();
    }


    @Override
    public void createStudent(Student student) {
        dataAPI.addStudent(student, new DataAPI.CreateStudentListener() {

            @Override
            public void onStudentCreated(Student student) {
                view.onStudentCreated(student);
            }

            @Override
            public void onStudentAlreadyExists(Student student) {

            }

            @Override
            public void onError(String message) {
                view.onError(message);
            }
        });
    }

    @Override
    public void getStudents() {
        listAPI.getAllStudents(new ListAPI.DataListener() {
            @Override
            public void onResponse(Response response) {
                view.onResponse(response);
            }

            @Override
            public void onError(String messsage) {
                view.onError(messsage);
            }
        });
    }


}
