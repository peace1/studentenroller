package co.za.amahle.studentregisterlib.api;

import android.content.Context;
import android.os.Parcelable;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Random;

import co.za.amahle.studentregisterlib.model.Response;
import co.za.amahle.studentregisterlib.model.Student;


public class DataAPI {
    public static final String TAG = DataAPI.class.getSimpleName();
    private FirebaseDatabase db;


    public DataAPI() {
        db = FirebaseDatabase.getInstance();
    }


    public interface CreateStudentListener {
        void onStudentCreated(Student student);

        void onStudentAlreadyExists(Student student);

        void onError(String message);
    }


    public static final String
            STUDENTS = "students";

    public void addStudent(final Student student, final CreateStudentListener listener) {
        Log.i(TAG, "***************** addStudent() " + student.getFullname());
        DatabaseReference userRef = db.getReference(STUDENTS);
        log(TAG, userRef);

        userRef.push().setValue(student, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError == null) {
                    Log.i(TAG, "***************** onComplete: student added to firebase: " + student.getFullname());
                    student.setStudentID(databaseReference.getKey());
                    listener.onStudentCreated(student);
                } else {
                    Log.e(TAG, "***************** onComplete: student not added to firebase: " + student.getFullname());
                    listener.onError(databaseError.getMessage());
                }

            }
        });
    }


    public static void log(String method, DatabaseReference ref) {
        Log.w(TAG, method.concat(" - databaseReference: " + ref.getRef().toString()));
    }


}
