package co.za.amahle.studentregisterlib.api;


import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import co.za.amahle.studentregisterlib.model.Response;
import co.za.amahle.studentregisterlib.model.Student;

public class ListAPI {

    public static final String TAG = ListAPI.class.getSimpleName();
    FirebaseDatabase db;

    public ListAPI() {
        db = FirebaseDatabase.getInstance();
    }

    public interface DataListener {
        void onResponse(Response response);

        void onError(String messsage);
    }

    public void getAllStudents(final DataListener listener) {
        Log.i(TAG, "***************** getAllStudents ");
        DatabaseReference ref = db.getReference(DataAPI.STUDENTS);
        Query q = ref.orderByKey();
        q.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.i(TAG, "***************** onDataChange: students: " + dataSnapshot.getKey());
                Response response = new Response();
                response.setStudents(new ArrayList<Student>());
                if (dataSnapshot.getChildrenCount() > 0) {
                    for (DataSnapshot shot : dataSnapshot.getChildren()) {
                        Student u = shot.getValue(Student.class);
                        u.setStudentID(shot.getKey());
                        response.getStudents().add(u);
                    }
                }
                response.setStutus(0);
                listener.onResponse(response);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "***************** onCancelled: Error: " + databaseError);
                listener.onError(databaseError.getMessage());
            }
        });
    }
}
