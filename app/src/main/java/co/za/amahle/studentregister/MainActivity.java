package co.za.amahle.studentregister;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import co.za.amahle.studentregisterlib.adapter.StudentAdapter;
import co.za.amahle.studentregisterlib.crud.CrudContract;
import co.za.amahle.studentregisterlib.crud.CrudPresenter;
import co.za.amahle.studentregisterlib.model.Response;
import co.za.amahle.studentregisterlib.model.Student;
import co.za.amahle.studentregisterlib.util.SimpleDividerItemDecoration;

public class MainActivity extends AppCompatActivity implements CrudContract.View {

    protected RecyclerView listStudent;
    protected StudentAdapter studentAdapter;
    protected Context ctx;
    protected CrudPresenter presenter;
    protected Response response;
    protected List<Student> students;
    protected View view;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "***************** onCreate ******************");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        ctx = getApplicationContext();
        view = getCurrentFocus();
        setView();
        presenter = new CrudPresenter(this);
    }

    private void setView() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AddStudentActivity.class);
                startActivity(intent);
                finish();
            }
        });
        listStudent = (RecyclerView) findViewById(R.id.listStudent);
        LinearLayoutManager llm = new LinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false);
        listStudent.setLayoutManager(llm);
        listStudent.addItemDecoration(new SimpleDividerItemDecoration(ctx));
        listStudent.setHasFixedSize(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG, "***************** onActivityResult ******************");
        if (resultCode == 1) {
            if (data == null) {
                return;
            }
            Student student = (Student) data.getSerializableExtra("student");
            students.add(student);
            studentAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.getStudents();
    }


    @Override
    public void onStudentCreated(Student student) {
        Log.i(TAG, "***************** onStudentCreated ******************");
        if (students == null) {
            students = new ArrayList<>();
        }
        students.add(student);
        studentAdapter.notifyDataSetChanged();
    }


    @Override
    public void onResponse(Response resp) {
        Log.i(TAG, "***************** onResponse ******************");
        if (resp == null) {
            return;
        }
        if (resp.getStutus() != 0) {
            return;
        }
        response = resp;
        students = response.getStudents();
        studentAdapter = new StudentAdapter(students, ctx, new StudentAdapter.StudentAdapterListener() {
            @Override
            public void onSelectedStudent(Student student) {
                Toast.makeText(ctx, student.getFullname(), Toast.LENGTH_SHORT).show();
            }
        });
        listStudent.setAdapter(studentAdapter);
    }

    @Override
    public void onError(String message) {
        Log.i(TAG, "***************** onError ******************");

    }

    static final String TAG = MainActivity.class.getName();
}
