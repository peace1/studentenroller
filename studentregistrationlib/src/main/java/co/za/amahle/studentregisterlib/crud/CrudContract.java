package co.za.amahle.studentregisterlib.crud;


import java.util.List;

import co.za.amahle.studentregisterlib.model.Response;
import co.za.amahle.studentregisterlib.model.Student;

/**
 * Created by sifiso.mtshweni on 2018/06/07.
 */
public class CrudContract {
    public interface Presenter {
        void createStudent(Student student);

        void getStudents();
    }

    public interface View {
        void onStudentCreated(Student student);

        void onResponse(Response response);

        void onError(String message);
    }
}
