package co.za.amahle.studentregisterlib.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import co.za.amahle.studentregisterlib.R;
import co.za.amahle.studentregisterlib.model.Student;

/**
 * Created by sifiso.mtshweni on 2018/06/07.
 */
public class StudentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Student> mList;
    private Context ctx;
    private StudentAdapterListener listener;


    public interface StudentAdapterListener {
        void onSelectedStudent(Student student);
    }

    public StudentAdapter(List<Student> mList, Context ctx, StudentAdapterListener listener) {
        this.mList = mList;
        this.ctx = ctx;
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.student_list_view, parent, false);
        return new StudentViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Student student = mList.get(position);
        String gender;
        final StudentViewHolder svh = (StudentViewHolder) holder;
        svh.txtfullName.setText(student.getFullname());

        if (student.getGender() == 1) {
            gender = "Male";
        } else {
            gender = "Female";
        }
        svh.txtGender.setText(gender);
        svh.txtAge.setText(student.getAge() + "");
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class StudentViewHolder extends RecyclerView.ViewHolder {
        protected TextView txtfullName, txtGender, txtAge;

        public StudentViewHolder(View itemView) {
            super(itemView);
            txtfullName = itemView.findViewById(R.id.txtfullName);
            txtGender = itemView.findViewById(R.id.txtGender);
            txtAge = itemView.findViewById(R.id.txtAge);
        }
    }
}
