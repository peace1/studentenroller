package co.za.amahle.studentregister;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import co.za.amahle.studentregisterlib.crud.CrudContract;
import co.za.amahle.studentregisterlib.crud.CrudPresenter;
import co.za.amahle.studentregisterlib.model.Response;
import co.za.amahle.studentregisterlib.model.Student;

public class AddStudentActivity extends AppCompatActivity implements CrudContract.View {

    CrudPresenter presenter;
    private RadioGroup rgGendr;
    private Integer gender = null;
    private EditText etName,  etAge;
    private Button btnSave, btnCancel;
    private Student student;
    private View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "***************** onCreate ******************");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        view = getCurrentFocus();
        presenter = new CrudPresenter(this);
        setView();
    }

    private void setView() {
        rgGendr = (RadioGroup) findViewById(R.id.rgGendr);
        rgGendr.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.male) {
                    gender = 1;
                } else {
                    gender = 0;
                }
            }
        });

        etName = (EditText) findViewById(R.id.etName);
        etAge = (EditText) findViewById(R.id.etAge);
        btnSave = (Button) findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateInput();
                student = new Student();
                student.setAge(Integer.parseInt(etAge.getText().toString()));
                student.setFullname(etName.getText().toString());
                student.setGender(gender);
                presenter.createStudent(student);
            }
        });
        btnCancel = (Button) findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void validateInput() {
        if (etName.getText().toString().isEmpty() ) {
            etName.setError("Input can't be empty");
            return;
        }
        if (gender == null) {
            return;
        }

        if (etAge.getText().toString().isEmpty()) {
            etAge.setError("Input can't be empty");
            return;
        }


    }

    @Override
    public void onStudentCreated(Student student) {
        Intent intent = new Intent(AddStudentActivity.this,MainActivity.class);
        intent.putExtra("student",student);
        startActivityForResult(intent,1);
        finish();
    }

    @Override
    public void onResponse(Response response) {

    }

    @Override
    public void onError(String message) {
        Log.i(TAG, "***************** onError ******************");

    }
    static  final  String TAG = MainActivity.class.getName();
}
