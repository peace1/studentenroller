package co.za.amahle.studentregisterlib.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.List;

/**
 * Created by sifiso.mtshweni on 2018/06/06.
 */
public class Response implements Serializable {
    public int stutus;
    public String message;
    private Student student;
    private List<Student> students;


    public Response() {

    }

    public int getStutus() {
        return stutus;
    }

    public void setStutus(int stutus) {
        this.stutus = stutus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }
}
